package com.example.root.lab53;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView nombrePlatillo;
    ListView listaIngredientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombrePlatillo = (TextView) findViewById(R.id.platillo);
        int index_comida  = getIntent().getIntExtra("PosComida", -1);
        //index_comida = 3;
        String recetario[] = getResources().getStringArray(R.array.recetario);
        listaIngredientes = (ListView) findViewById(R.id.lista_ingredientes);

        if(index_comida != -1);
        if(index_comida*5 < recetario.length){
            nombrePlatillo.setText(recetario[index_comida*5]);
            ArrayList<String> ingredientes = new ArrayList<>();
            ingredientes.add(recetario[index_comida*5+1]);
            ingredientes.add(recetario[index_comida*5+2]);
            ingredientes.add(recetario[index_comida*5+3]);
            ingredientes.add(recetario[index_comida*5+4]);
            ArrayAdapter<String> Mingredientes = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, ingredientes);
            listaIngredientes.setAdapter(Mingredientes);
        }
        else{
            nombrePlatillo.setText("Busca un Restaurant");
            nombrePlatillo.setTextSize(20);
            ArrayList<String> ingredientes = new ArrayList<>();
            ingredientes.add("Bembos");
            ingredientes.add("Burguer King");
            ingredientes.add("El ultimo Refugio");
            ingredientes.add("Tia Grasa");
            ArrayAdapter<String> Mingredientes = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, ingredientes);
            listaIngredientes.setAdapter(Mingredientes);
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_receta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
